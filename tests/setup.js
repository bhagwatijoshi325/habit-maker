const mongoose = require('mongoose');
const {server } = require("../index");

beforeEach(async () => {
  for (var i in mongoose.connection.collections) {
    await mongoose.connection.collections[i].deleteMany({});
  }
});

afterAll(async () => {
  await mongoose.connection.close();
  server.close();
});