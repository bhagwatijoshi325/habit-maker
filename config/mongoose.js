// require the mongoose
const mongoose = require("mongoose");

// connect to the database
console.log('process.env.NODE_ENV: ', process.env.NODE_ENV)
if (process.env.NODE_ENV === 'test') {
    mongoose.connect(process.env.DATABASE_TEST);
  } else if (process.env.NODE_ENV === 'dev') {
    mongoose.connect(process.env.DATABASE_DEV);
  } else if (process.env.NODE_ENV === 'debug') {
    mongoose.connect(process.env.DATABASE_DEV);
  }

// acquire the connection(to check if it is successful
const db = mongoose.connection;

// error
db.on("error", console.log.bind(console, "error connecting to the database"));

// up and running then print message
db.once("open", function(){
    console.log("Successfully connected to the database");
});