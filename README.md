# Habit Monitoring web application
This Habit Monitoring web application is a robust solution developed using Node.js, CSS, EJS, MongoDB, Express, and Mongoose. It empowers users to monitor and manage their habits effectively, fostering positive routines and facilitating the achievement of their objectives.

## Key Functionalities:

- Incorporate multiple habits for monitoring such as reading, exercising, etc.
- Daily tracking of each habit. Habits can have one of the three statuses:
  - Completed 
    - Mark the habit as completed for the day
  - Incomplete 
    - Mark the habit as not completed for the day
  - Neutral 
    - No action taken by the user for the habit on a particular day
- A view to display all current habits with an option to add a new habit for tracking.
- A view to exhibit a 7-day record for each habit

## Technologies Employed:

- Node.js: Provides the runtime environment for the web application.
- CSS: Facilitates visually pleasing and responsive design.
- EJS: Enables dynamic view rendering, enhancing the presentation of habit-related data.
- MongoDB: A NoSQL database used for dependable data storage and retrieval.
- Express: A web application framework for managing HTTP requests and responses.
- Mongoose: A sophisticated MongoDB object modeling tool for database interaction.


## Requirements

For development, you will only need Node.js (16+), a node global package (Npm), and mongoDB atlas URI or locally installed mongodb server.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

  If the installation was successful, you should be able to run the following command.

    $ node --version
    V16.xx.xx

    $ npm --version
    x.xx.xx

  If you need to update `npm`, you can make it using `npm` !Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g


## Install Project

    $ git clone https://gitlab.com/bhagwatijoshi325/habit-maker.git
    $ cd habit-maker
    $ npm install

## Running the project on development
    # Set up .env file in the root, format is given in the .env.example
    
    $ npm run start:dev


## Folder Structure
```
[HABIT-MAKER-APP]
│   .dockerignore
│   .env
│   .gitignore
│   docker-compose.dev.yml
│   docker-compose.prod.yml
│   Dockerfile.dev
│   Dockerfile.prod
│   index.js
│   jest.config.js
│   package-lock.json
│   package.json
│   Procfile
│   README.md
│
├───.devcontainer
│       devcontainer.json
│       docker-compose.yml
│
├───.vscode
│       launch.json
│
├───assets
│   │   Screenshot 2023-07-23 094142.png
│   │   weekly view.png
│   │
│   ├───css
│   │       home.css
│   │
│   ├───js
│   │       home.js
│   │
│   └───sass
│           home.scss
│
├───config
│       mongoose.js
│
├───controllers
│       home_controller.js
│
├───model
│       habit.js
│
├───routes
│       index.js
│
├───tests
│   │   setup.js
│   │
│   └───routes
│           index.test.js
│
└───views
        home.ejs
        layout.ejs
        _daily.ejs
        _weekly.ejs
```


## Support

Contributions, bug reports, and feature requests are welcome. Feel free to fork the repository and submit pull requests to contribute to the project.