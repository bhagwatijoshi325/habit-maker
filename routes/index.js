// Import express
const express = require("express");

// Create a new router
const router = express.Router();

// Import the home controller
const homeController = require("../controllers/home_controller");

// Route for the home page
router.get("/", homeController.home);

// Route for adding a new habit
router.post("/add-habit", homeController.addHabit);

// Route for updating the status of a habit
router.get("/update-status", homeController.updateHabitStatus);

// Route for deleting a habit
router.delete("/delete-habit/:id", homeController.deleteHabit);

// Route for changing the view (daily or weekly)
router.post("/change-view", homeController.changeView);

// Route for updating the status of a habit for a specific week
router.get("/update-status-weekly", homeController.updateHabitStatusWeekly);

// Export the router
module.exports = router;