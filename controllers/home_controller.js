// Import Habit model
const Habit = require("../model/habit");

// Variables to control the initial state and view of the application
let firstTime = true;
let view = "Daily";

// Controller to handle home route
module.exports.home = async function (req, res) {
    try {
        // Fetch all habits from the database
        const allHabits = await Habit.find({});

        // If it's the first time the app is run, add a record for each habit
        if (firstTime) {
            for (let habit of allHabits) {
                let currentDate = new Date();
                if (habit.records?.[habit.records.length - 1]?.date?.getDate() != currentDate.getDate()) {
                    habit.records.push({
                        date: currentDate,
                        status: "None"
                    });
                    habit.save();
                }
            }
            firstTime = false;
        }

        // Render the home view with all habits
        return res.render("home", {
            allHabits: allHabits,
            view: view
        } );

    } catch (err) {
        console.log(err);
        res.redirect("back");
    }
}

// Controller to handle adding a new habit
module.exports.addHabit = async function (req, res) {
    try {
        // Create a new habit in the database
        const habit = await Habit.create({
            name: req.body.habit,
            records: []
        });

        // Add records for the past 7 days
        for (let i = 0; i < 7; i++) {
            let currentDate = new Date();
            habit.records.unshift(getPreviousDatesRecords(currentDate, i));
        }

        // Save the habit and redirect to the home page
        habit.save();
        return res.redirect("/");
    } catch (err) {
        console.log(err);
        return res.redirect("back");
    }
}

// Controller to handle updating a habit's status
module.exports.updateHabitStatus = async function (req, res) {
    try {
        // Find the habit in the database and update its status
        const habit = await Habit.findById(req.query.habitId);
        habit.records[habit.records.length - 1].status = req.query.status;
        habit.save();

        // Return a success message
        return res.status(200).json({
            message: "status updated"
        });
    } catch (err) {
        console.log(err);
        return res.redirect("back");
    }
}

// Controller to handle deleting a habit
module.exports.deleteHabit = async function (req, res) {
    try {
        // Find the habit in the database and delete it
        const habit = await Habit.findByIdAndDelete(req.params.id);

        // Return a success message
        return res.status(200).json({
            message: "Habit deleted Successfully"
        });
    } catch (err) {
        console.log(err);
        return res.redirect("back");
    }
}

// Controller to handle updating a habit's status for a specific week
module.exports.updateHabitStatusWeekly = async function (req, res) {
    try {
        // Find the habit in the database
        const habit = await Habit.findById(req.query.habitId);

        // Update the status of the habit for the specific date
        for (let i = habit.records.length - 1; i >= habit.records.length - 6; i--) {
            if (habit.records[i].date.toISOString() === req.query.date) {
                habit.records[i].status = req.query.status;
                habit.save();
                break;
            }
        }

        // Return a success message
        return res.status(200).json({
            message: "status update on specific date is successful"
        });
    } catch (err) {
        console.log(err);
        return res.redirect("back");
    }
}

// Controller to handle changing the view (daily OR Weekly)
module.exports.changeView = function (req, res) {
    view = view === "Daily" ? "Weekly" : "Daily";
    res.redirect("/"); 
}

// Function to get previous dates
function getPreviousDatesRecords(currentDate, i) {
    const record = {};
    record.date = currentDate.setDate(currentDate.getDate() - i);
    record.status = "None";
    return record;
}
