require("dotenv").config();
const express = require("express");
const path = require("path");
const db = require("./config/mongoose");
const sassMiddleware = require("node-sass-middleware");
const { request } = require("http");
const expressLayout = require('express-ejs-layouts');
const app = express();

let port;;
if (process.env.NODE_ENV === 'test') {
    port = process.env.TEST_PORT;
  } else if (process.env.NODE_ENV === 'dev') {
    port = process.env.DEV_PORT;
  } else if (process.env.NODE_ENV == 'debug'){
    port = process.env.DEBUG_PORT
  }

// body parser
app.use(express.urlencoded({
    extended: true
}));

app.use( (req, res , next) => {
    console.log("url : " , req.originalUrl);
    console.log("method : ", req.method);
    console.log("params : ", req.params);
    console.log("headers : ", req.headers);
    console.log("body : ", req.body);
    next();
})

// set up the sass middleware
app.use(sassMiddleware({
    src: path.join(__dirname, "assets", "sass"),
    dest: path.join(__dirname, "assets", "css"),
    // debug: true,
    outputStyle: "expanded",
    prefix: "/css"
}));

// tell express which template engine we are using
app.set("view engine", "ejs");

// set template paths for view 
app.set("views", path.join(__dirname, "views"));

// ------ EJS layouts ------//
app.use(expressLayout);
app.set('layout extractStyles',true);
app.set('layout extractScripts',true);

// set the path of all static file
app.use(express.static("assets"));

// use express routes
app.use("/", require("./routes/index"));

// express is listening on port 8000
const server = app.listen(port, function(err){
    if(err){
        console.log("Error in running the server", err);
        return;
    }
    console.log(`my express server is running on ${port}`);
});


module.exports =  {
    app,
    server
};
