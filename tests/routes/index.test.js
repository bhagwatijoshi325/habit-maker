const request = require('supertest');
const {server} = require('../../index');
const Habit = require('../../model/habit');

test('on homepage should give 200 status code and text/html content in response.', async () => {
  await Habit.deleteMany({}); 
  const res = await request(server).get('/');
  expect(res.statusCode).toEqual(200);
  expect(res.headers['content-type']).toContain('text/html; charset=utf-8')
}); 
